package de.idensitylab.cucumber;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.plugin.testing.WithoutMojo;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * This is the JUnit test runner class for the plugin project.
 * <br>The plugin is run with the testdata included to src/test/resources/project-to-test
 *
 * @author Wolfgang Hierl
 */
public class CucumberFeaturesCSVReplacerTest
{

    CucumberFeaturesReplacer cfr;
    CucumberFeaturesCSVReplacer cfcr;
    File fFeature;
    File fOut, csvDir;
    Log log;

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
        }
    };


    /**
     * @throws Exception if any
     */
    @Test
    public void testPlugin()
            throws Exception {
        File pom = new File("target/test-classes/project-to-test/");
        assertNotNull(pom);
        assertTrue(pom.exists());

        CucumberFeaturesCSVReplacer myReplacer = (CucumberFeaturesCSVReplacer) rule.lookupConfiguredMojo(pom, "csv-replacer");
        assertNotNull(myReplacer);
        myReplacer.execute();

        File featuresOutputDirectory = (File) rule.getVariableValueFromObject(myReplacer, "featuresOutputDirectory");
        assertNotNull(featuresOutputDirectory);
        assertTrue(featuresOutputDirectory.exists());
    }

//    /**
//     * @throws Exception if any
//     */
//    @Test
//    public void testHelpMojo()
//            throws Exception {
//        File pom = new File("target/test-classes/project-to-test/");
//        assertNotNull(pom);
//        assertTrue(pom.exists());
//
//        CucumberFeaturesCSVReplacer hm = (CucumberFeaturesCSVReplacer) rule.lookupConfiguredMojo(pom, "help");
//        assertNotNull(hm);
//        hm.execute();
//
////
////        File touch = new File( outputDirectory, "touch.txt" );
////        assertTrue( touch.exists() );
//
//    }

    /**
     * Do not need the MojoRule.
     */
    @WithoutMojo
    @Test
    public void testCucumberFeaturesReplacer() {
        log = new SystemStreamLog();
        fFeature = new File("src/test/resources/project-to-test/target/features/replacePluginTestDatatable.feature");
        fOut = new File("target/test-classes/test1.feature");
        csvDir = new File("target/test-classes/project-to-test/target/data");
        cfr = new CucumberFeaturesReplacer(log, ";", "#@csv:", "dev");

        boolean ck = cfr.replaceStringInFeature(fFeature, fOut, csvDir);
        assertTrue(ck);
        assertTrue(fOut.exists());
    }

}

