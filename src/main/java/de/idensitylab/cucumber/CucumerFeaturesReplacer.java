package de.idensitylab.cucumber;

import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Helper class to add testdata from csv to feature file
 *
 * @author Wolfgang Hierl
 */
class CucumberFeaturesReplacer {
    private Log log;
    private String regex, delimiter, env;

    public CucumberFeaturesReplacer(Log log, String delimiter, String regex, String env) {
        this.log = log;
        this.delimiter = delimiter;
        this.regex = regex;
        this.env = env;
    }

    /**
     * Replace founded tags in feature file
     *
     * @param feature  is the File Object to scan for tags to replace
     * @param fOutFile is the paath for the new feature file with replaced tags
     * @param csvDir   is the directory where the csv files are placed
     * @return true, if a tag was found and replaced
     */
    public boolean replaceStringInFeature(File feature, File fOutFile, File csvDir) {
        String replacedContent;
        boolean ck = false;

        try (PrintWriter out = new PrintWriter(fOutFile)) {
            replacedContent = readFeatureCsv(feature,csvDir);
            if (replacedContent!=null) {
                out.println(replacedContent);
                ck = true;
            }
        } catch (IOException e) {
            log.error("\n### ERROR in '" + Class.class.getName() + "' ###");
            log.error(e.getMessage());
            ck = false;
        }
        return ck;
    }


    private String readFeatureCsv(File feature, File csvDir) {

        String line;
        StringBuilder newContent = new StringBuilder();
        boolean ckConverted = false;

        try (Scanner fileScan = new Scanner(feature)) {

            // Read and process each line of the file
            while (fileScan.hasNext()) {
                line = fileScan.nextLine();

                if (line.contains(this.regex)) {
                    log.info("Tag to convert found in " + feature.getName());
                    // replace test env
                    String envTag = "<ENV>";
                    if (line.contains(envTag))
                        line = line.replace(envTag, env);

                    File csvPath = new File(csvDir + File.separator + line.substring(line.indexOf(this.regex) + this.regex.length()).trim());

                    log.info("CSV-Filepath in Feature-File: " + csvPath);
                    newContent.append(this.readCSVFile(csvPath)).append("\n");
                    ckConverted = true;
                } else {
                    newContent.append(line).append("\n");
                }
            }
        } catch (IOException e) {
            log.error("ERROR reading feature-file: " + e.getMessage());
            return null;
        }

        return (ckConverted?newContent.toString():null);
    }

    /**
     * read data from csv file
     *
     * @param csvPath   is Filepath to the csv file to read
     * @return String with testdata from csv file in table format to use in cucumber feature file
     */
    private String readCSVFile(File csvPath) {
        String line;
        StringBuilder content = new StringBuilder();

        try (Scanner fileScan = new Scanner(csvPath)) {
            // Read and process each line of the file
            while (fileScan.hasNext()) {
                line = fileScan.nextLine();
                content.append("\t| ").append(line.replaceAll(this.delimiter, "\t| ")).append("\t|\n");
            }
            return content.toString();
        } catch (Exception e) {
            this.log.error(e.getMessage());
            return e.getMessage();
        }
    }
}
