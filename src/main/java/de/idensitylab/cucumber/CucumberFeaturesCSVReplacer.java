package de.idensitylab.cucumber;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.*;
import java.nio.file.Files;
import java.util.Objects;

/**
 * Maven Plugin class for Goal csv-replacer.
 * The plugin replaces csv tags in cucumber feature files with csv-data.
 */
@Mojo( name = "csv-replacer", defaultPhase = LifecyclePhase.PROCESS_SOURCES )
public class CucumberFeaturesCSVReplacer
    extends AbstractMojo
{
    /**
     * plugin configuration parameter to set the folder with the testdata (csv files)
     */
    @Parameter( defaultValue = "src/test/resources/testdata", property = "csv-replacer.testdataDirectory" )
    private File testdataDirectory;

    /**
     * plugin configuration parameter to set folder of cucumber feature files
     */
    @Parameter( defaultValue = "src/test/resources/features", property = "csv-replacer.featuresInputDirectory", required = true )
    private File featuresInputDirectory;

    /**
     * plugin configuration parameter to set folder to save converted cucumber feature files
     */
    @Parameter( defaultValue = "src/test/resources/features-converted", property = "csv-replacer.featuresOutputDirectory" )
    private File featuresOutputDirectory;

    /**
     * plugin configuration parameter to set tag to use for replace with csv testdata
     */
    @Parameter( defaultValue = "#@csv:", property = "csv-replacer.replaceString" )
    private String replaceString;

    /**
     * plugin configuration parameter to set delimiter of csv testdata
     */
    @Parameter( defaultValue = ";", property = "csv-replacer.delimiter" )
    private String delimiter;

    /**
     * plugin configuration parameter to set Testenv for csv testdata
     */
    @Parameter( defaultValue = "dev", property = "csv-replacer.testenv" )
    private String testenv;

    CucumberFeaturesReplacer cfr;

    /**
     * main method of maven plugin.
     * @throws MojoExecutionException for maven plugin
     */
    public void execute()
        throws MojoExecutionException
    {
        getLog().info("######################################################");
        getLog().info("###  Starting Cucumber Preprocessor csv-replacer.  ###");
        getLog().info("######################################################\n");

        File fIn = featuresInputDirectory;
        File fOut = featuresOutputDirectory;
        File fCSVDir = testdataDirectory;
        String regex = replaceString;
        String delimit = delimiter;
        String env = testenv;

        File[] featureFiles;

        try {
            // checks InputDir
            featureFiles = this.checkInputDir(fIn);

            // checks outputDir
            this.checkCleanOutputDir(fOut);

            cfr = new CucumberFeaturesReplacer(getLog(),delimit,regex, env);
        }
        catch ( IOException e )
        {
            throw new MojoExecutionException( e.getMessage(), e );
        }

        // convert files
        int converterCount=0;

        for (File featureFile : featureFiles) {
            // count, if something was replaced in file
            if (this.convertFeatureCSV(featureFile, new File(fOut.getAbsolutePath() + File.separator + featureFile.getName()), fCSVDir)) {
                converterCount++;
            }
        }

        getLog().info("Converted Files: " + converterCount);
    }

    /**
     * create a new feature file and replace tags with content of csv file
     * @param feature feature file to convert
     * @param fOutFile path to new feature file
     * @param fCSVDir path to testdata cssv file
     * @return return true, if a tag was found and file was converted
     * @throws MojoExecutionException for maven plugin exception
     */
    private boolean convertFeatureCSV(File feature, File fOutFile, File fCSVDir) throws MojoExecutionException {

        getLog().info("Convert File to: " + fOutFile);
        // Scan feature file for csv-file tag
        if(cfr.replaceStringInFeature(feature,fOutFile,fCSVDir)) {
            return true;
        } else{
            // copy file if nothing to convert
            try {
                copyFileUsingStream(feature, fOutFile);
            } catch (IOException e) {
                throw new MojoExecutionException("Error while copy featureFile: " + fOutFile.getName() + " | " + e.getMessage());
            }
            return false;
        }
    }

    /**
     * outputs a File Array
     */
    private void outputFileList(File[] flist) {
        for (File aFlist : flist) {
            getLog().debug(aFlist.getName());
        }
    }

    private File[] checkInputDir(File fIn) throws MojoExecutionException {
        File[] featureFiles;

        if ( !fIn.exists() )
        {
            throw new MojoExecutionException( "Error Parameter featuresInputDirectory: '" + fIn + "' does not exists.");
        } else {
            featureFiles = fIn.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".feature");
                }
            });

            if(featureFiles != null && featureFiles.length<1) {
                throw new MojoExecutionException( "Error Parameter featuresInputDirectory: no files with extension 'feature' found.");
            } else {
                getLog().info("Feature-Files found to check for converting:");
                this.outputFileList(Objects.requireNonNull(featureFiles));
            }
        }

        return featureFiles;
    }

    private void checkCleanOutputDir(File fOut) throws MojoExecutionException, IOException {
        if ( !fOut.exists() )
        {
            if(fOut.mkdirs()) {
                getLog().info("featuresOutputDirectory created.");
            } else {
                throw new MojoExecutionException( "Could not create featuresOutputDirectory: "+ fOut);
            }
        } else {
            // clean outputDir
            cleanFolder(fOut);
        }
    }

    private void cleanFolder(File folder) throws IOException {
        if(Objects.requireNonNull(folder.listFiles()).length>0) {
            getLog().info("Cleaning ");
            for (File file : Objects.requireNonNull(folder.listFiles()))
                if (!file.isDirectory())
                    Files.delete(file.toPath());
        }
    }

    /**
     * copy file to new destination
     */
    private static void copyFileUsingStream(File source, File dest) throws IOException {

        try (
                InputStream is = new FileInputStream(source);
             OutputStream os = new FileOutputStream(dest)
        ){
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
    }
}
