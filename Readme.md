# Cucumber Features CSV Replacer *cucumber-features-pp-maven-plugin*
A Maven Plugin to use CSV Testdata in Cucumber Featurefiles. 
All feature files will be copied from your configured `featuresInputDirectory` to 
directory `featuresOutputDirectory` and replace the included tags with content of referenced csv file.
The plugin is executed before maven goal *compile* is executed.

## Plugin Parameters
The plugin parameters listed here can be used in configurations area of your maven pom file.

The paths of directories are all relative to your java project directory.
All parameter are **optional**.

 | Parameter                | Default value                           | Description                                   |
 | ------------------------ |:---------------------------------------:| ---------------------------------------------:|
 | featuresInputDirectory   | `src/test/resources/features`           | directory with your feature files             |
 | featuresOutputDirectory  | `src/test/resources/features-converted` | directory for converted feature files         |
 | testdataDirectory        | `src/test/resources/testdata`           | directory of feature files                    |
 | replaceString            | `#@csv:`                                | tag to use path to csv-files in feature files |
 | delimiter                | `;`                                     | delimiter in your csv-files                   |
 | testenv                  | `dev`                                   | test env to replace in <ENV> Tag of CSV-path  |
  
## Quick start

### Add dependency for plugin to pom file
In the dependencies section of your pom file add the dependency for this plugin.
```
<dependency>
    <groupId>org.bitbucket.idensitylab</groupId>
    <artifactId>cucumber-features-pp-maven-plugin</artifactId>
    <version>1.0.0</version>
</dependency>
```

### Add the plugin to your pom file of your java project.
In the plugins section of your pom file add a new plugin block like this and change configuration parameters.
```
<plugin>
    <groupId>org.bitbucket.idensitylab</groupId>
    <artifactId>cucumber-features-pp-maven-plugin</artifactId>
    <version>1.0.0</version>
    <configuration>
        <featuresInputDirectory>src/test/resources/features</featuresInputDirectory>
        <featuresOutputDirectory>src/test/resources/features-converted</featuresOutputDirectory>
        <testdataDirectory>src/test/resources/testdata</testdataDirectory>
    </configuration>
    <executions>
        <execution>
            <phase>compile</phase>
            <goals>
                <goal>csv-replacer</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

### Prepare your testdata in your csv files.
Create a directory for your testdata in e.g.
```
<project.directory>/src/test/resources/testdata
``` 
This path should be set relativ to your project directory in parameter *testdataDirectory* in your pom file.
Use no delimiter at the end of the line.
The testdata files to **use as datatable in scenario should NOT include a header line**.

Create e.g. a csv file `testdata1.csv` like this example with 3 columns and delimiter `;`:
```
Hans;4;23
Berti;3;54
Claus;2;67
Dieter;7;18
```
The testdata files to **use in example section of a cucumber scenario must include a header line**.
```
name;id;age
Hans;4;23
Berti;3;54
Claus;2;67
Dieter;7;18
```
 
### Prepare your cucumber featurefiles
Replace your data in your feature files with the Tag with the path to the csv testdata.
Cucumber Feature file before adding csv-tags for plugin.
```
Feature: Favorites
  Background:
    Given Open Application and Enter url
    Then I see a browser title containing "GeonamesApp"

  Scenario Outline: test csv replacment in example
    * I forcefully click on element having name "searchNav"
    * I input "<cityname>" and search
    * wait 3 seconds until jquery is done
    * element having css "#searchResultsList app-geo-listitem h5" should have partial text as "<cityname>"
    * I wait for 2 sec
    * I take screenshot
  Examples:
    | cityname  |
    | München   |
    | Berlin    |

  Scenario: test csv replacment in datatable
    * I forcefully click on element having name "searchNav"
    * I input city and search
      | München   |
      | Berlin    |
    * I wait for 2 sec
    * I take screenshot
```

Cucumber Feature file prepared to use testdata from csv file.
```
Feature: Favorites
  Background:
    Given Open Application and Enter url
    Then I see a browser title containing "GeonamesApp"

  Scenario Outline: test csv replacment in example
    * I forcefully click on element having name "searchNav"
    * I input "<cityname>" and search
    * wait 3 seconds until jquery is done
    * element having css "#searchResultsList app-geo-listitem h5" should have partial text as "<cityname>"
    * I wait for 2 sec
    * I take screenshot
  Examples:
    #@csv:testdata1.csv

  Scenario: test csv replacment in datatable
    * I forcefully click on element having name "searchNav"
    * I input city and search
      #@csv:testdata2.csv
    * I wait for 2 sec
    * I take screenshot
```

## Environment specific testdata
If you need to handle environment or e.g. customer specific testdata, you can handle the testdata in different files.
You can then use the <ENV> tag in the csv filepathes of your featurefiles.
You can duplicate your testdata1.csv and rename to e.g. dev.testdata1.csv and prod.testdata1.csv.

```
* I input city and search
      #@csv:<ENV>.testdata1.csv
```

## Project info
The project is still in alpha version and not publish in maven repo.
The use of this plugin and code is free and uses [MIT License](https://opensource.org/licenses/MIT).
